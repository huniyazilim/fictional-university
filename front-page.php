<?php 
    get_header(); 
    $theme_uri = get_theme_file_uri();

    function uri($uri) {
        echo $GLOBALS["theme_uri"].$uri;
    }
    
?>

<div class="page-banner">
    <!-- <div class="page-banner__bg-image" style="background-image: url(images/library-hero.jpg);"></div> -->
    <div class="page-banner__bg-image" style="background-image: url(<?php echo uri('/images/library-hero.jpg') ?>"></div>
    <div class="page-banner__content container t-center c-white">
        <h1 class="headline headline--large">Hoşgeldiniz!</h1>
        <h2 class="headline headline--medium">Öğrenci dostu kampüs</h2>
        <h3 class="headline headline--small">Siz sadece geleceğinize yoğunlaşın.</h3>
        <a href="<?php echo get_post_type_archive_link("program"); ?>" class="btn btn--large btn--blue">Find Your Major</a>
    </div>
</div>

<div class="full-width-split group">
    <div class="full-width-split__one">
        <div class="full-width-split__inner">
            <h2 class="headline headline--small-plus t-center">Upcoming Events</h2>

            <?php 
                $today = date('Ymd'); // bu format custom event_date alanın döndürdüğü format ile aynı!
                $events = new WP_Query(array(
                    'post_type' => "event",
                    'posts_per_page' => 2, // sayfa başına sadece iki kayıt getir.
                    // 'posts_per_page' => -1, // Bana bu sorgu koşullarına uyan tüm kayıtları getir!
                    // 'order_by' => 'title', // 'post_date' is the default, 'rand' yazarsan rastgele sıralama yapar :)
                    'order' => 'ASC', // DESC is the default
                    'meta_key' => 'event_date', // Hangi custom fielda göre sıralama yapacağız
                    'order_by' => 'meta_value_num', 
                    'meta_query' => array( // Burada birden fazla array ekleyerek çoklu koşul belirtebilirsin.
                        array (
                            'key' => 'event_date', // give us the posts which the event_date field values
                            'compare' => '>=', // are equal or greater than
                            'value' => $today, // todays date!,
                            'type' => 'numeric'
                        )
                        // , array('ikinci bir koşul')
                        // , array('üçüncü koşul')
                    )
                ));                

                while($events->have_posts()) {
                    $events->the_post(); 
                    // Burada get_template_part'ın ikinci argümanı dash kısmı, bu kısım ilk bölümün sonuna eklenerek
                    // şablonun çekileceği dosyanın dinamik olarak seçilmesi sağlanabilir.
                    get_template_part('template-parts/content', 'event');                    
                } // end while
                wp_reset_postdata();
            ?>
            <p class="t-center no-margin">
                <a href="<?php echo get_post_type_archive_link("event"); ?>" class="btn btn--blue">View All Events</a>
            </p>
        </div>
    </div>

    <div class="full-width-split__two">
        <div class="full-width-split__inner">
            <h2 class="headline headline--small-plus t-center">From Our Blogs</h2>

            <?php 
            
                $recentBlogs = new WP_Query(array(
                    'posts_per_page'=> 2
                ));

                while($recentBlogs-> have_posts()) {
                    $recentBlogs->the_post(); ?>
                    
                    <div class="event-summary">
                        <a class="event-summary__date event-summary__date--beige t-center" href="#">
                            <span class="event-summary__month"><?php the_time('M'); ?></span>
                            <span class="event-summary__day"><?php the_time('d'); ?></span>
                        </a>
                        <div class="event-summary__content">
                            <h5 class="event-summary__title headline headline--tiny">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
                            <p> 
                                <?php echo wp_trim_words(get_the_content(), 18); ?> 
                                <a href="<?php the_permalink(); ?>" class="nu gray">Read more</a>
                            </p>
                        </div>
                    </div>                    

                <?php } // end while
                    wp_reset_postdata(); // custom queryden sonra ana döngü için gerekli şeyleri başa alıyor.
                ?>

             <p class="t-center no-margin"><a href="<?php echo esc_url(site_url('/posts')); ?> " class="btn btn--yellow">View All Blog Posts</a></p>
        </div>
    </div>
</div>

	<div class="hero-slider">

      <div data-glide-el="track" class="glide__track">

        <div class="glide__slides">

		<?php 
	$slides = new WP_Query(array(
		'post_type' => 'slide',
		'posts_per_page' => -1,
		'meta_key' => 'slide_order',
		'order_by' => 'meta_value_num',
		'order' => 'ASC',
		'meta_query' => array( // Birden fazla koşu verebilmek için dizi
			array(
				'key' => 'aktif',
				'compare' => '>' ,
				'value' => 0,
				'type' => 'numeric'
			)
		)
	));

	while($slides->have_posts()) {
		$slides->the_post();
?>		
          
		<div class="hero-slider__slide" style="background-image: url(<?php uri(get_field('slide_image_url')) ?>);">
		<div class="hero-slider__interior container">
			<div class="hero-slider__overlay">
			<h2 class="headline headline--medium t-center"><?php the_title(); ?></h2>
			<p class="t-center"><?php echo get_field("slide_sub_title"); ?></p>
			<p class="t-center no-margin"><a href="#" class="btn btn--blue">Learn more</a></p>
			</div>
		</div>
		</div>

<?php 
	} // end while
	wp_reset_postdata();
?>

        </div>

        <div class="slider__bullets glide__bullets" data-glide-el="controls[nav]"></div>

      </div>

    </div>

<?php 
    $pages = get_pages();
    foreach ($pages as $page) {
        // echo $page->post_title;
        // var_dump($page->to_array()); // dizi içeriğini yazdır.
        foreach ($page->to_array() as $key => $value) { 
            // echo $key." : ".$value;            
        }
    }
    get_footer();
?>