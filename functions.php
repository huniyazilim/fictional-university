<?php

    require get_theme_file_path('/includes/like-route.php');
    require get_theme_file_path('/includes/search-route.php');

    // rest apiden dönen alanları özelleştirmek için...
    function university_custom_rest() {
        register_rest_field('post', 'authorName', array(
            // bu alanda dönecek bilgiyi bir fonk aracılığıyla döndürebilirsin, harika!!
            'get_callback' => function () {
                return get_the_author();
            }
        ));

        // Bu şekilde custom olarak eklenen alanları da dönebilirsin.
        register_rest_field('campus', 'mapLocation', array(
            'get_callback' => function () {
                return get_field("map_location");
            }
        ));

        // Kullanıcı kaç adet not almış şimdiye kadar.
        register_rest_field('note', 'userNoteCount', array(
            'get_callback' => function () {
                return count_user_posts(get_current_user_id(), 'note');
            }
        ));

        register_rest_field('note', 'maxAllovedNoteCount', array(
            'get_callback' => function () {
                return 5;
            }
        ));

    }
    
    // rest api devreye alındığında bu fonksiyonu çağır...
    add_action('rest_api_init', 'university_custom_rest');

    function stripPrivate($str) {
        return str_replace("Private: ", '',$str);
    }

    // Varsayılan değer veriyoruz ki, bu fonk array gönderilmeden çağırıldığında sıkıntı çıkmasın!
    function pageBanner($props = NULL) {
        // $result = $initial ?: 'default';
        $props['title'] = $props['title'] ?: get_the_title();
        $props['subtitle'] = $props['subtitle'] ?: get_field('page_banner_subtitle'); 
        
        // Ana sayfada değilsek ve bir arşiv sayfasında da değilsek.
        if (!$props['photo']) {
            if (get_field('page_banner_background_image') AND !is_archive() AND !is_home() ) {
              $props['photo'] = get_field('page_banner_background_image')['sizes']['pageBanner'];
            } else {
              $props['photo'] = get_theme_file_uri('/images/ocean.jpg');
            }
          }
    ?>

        <div class="page-banner">
            <div class="page-banner__bg-image" 
                style="background-image: url(<?php echo $props['photo']; ?>"></div>
                <div class="page-banner__content container container--narrow">
                    <h1 class="page-banner__title"><?php echo $props['title']; ?></h1>
                    <div class="page-banner__intro">
                        <p><?php echo $props['subtitle'] ?></p>
                    </div>
                </div>  
            </div>
        </div>        

    <?php } // end function pagebBanner

    function load_css_files() {
        wp_enqueue_style("custom-google-fonts", "//fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i|Roboto:100,300,400,400i,700,700i");
        wp_enqueue_style("font-awsome", "//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css");
        wp_enqueue_style("main_css_file", get_stylesheet_uri());
    }

    function load_js_files() {
        // wp_enqueue_script("test_js", "$theme_uri/test.js", [], false, true);
        
        // wp_enqueue_script('googleMap', '//maps.googleapis.com/maps/api/js?key=yourkeygoeshere', NULL, '1.0', true);
        // wp_enqueue_script('axios', '//cdn.jsdelivr.net/npm/axios/dist/axios.min.js', NULL, '1.0', true);
        wp_enqueue_script('glidejs', '//cdn.jsdelivr.net/npm/@glidejs/glide', NULL, '1.0', true);
      
        wp_enqueue_script('main-university-js', get_theme_file_uri('/scripts.js'), array('jquery'), '1.0', true);

        wp_localize_script('main-university-js', "themeData", array(
            'root_url' => get_site_url(),
            'author' => "Nadir Özkan",
            'nonce' => wp_create_nonce('wp_rest'), // wp tarafından sadece mevcut oturumda geçerli olacak eşsiz bir metin
            // Bu metin wp rest işlemleri için gerekli otantikasyonu sağlıyor
        ));
    }

    // add_action("action_name","function_to_be_called");
    add_action("wp_enqueue_scripts","load_css_files");
    add_action("wp_enqueue_scripts","load_js_files");

    function theme_actions() {
        register_nav_menu("headerMenuLocation", "Header Menu Location");
        add_theme_support('title-tag'); // otomatik başlık desteği ekle...
        add_theme_support('post-thumbnails'); // posta featured image desteği ekle...
        add_image_size('profLandscape', 400, 260, true); // image size name, width, height, crop(t/f) 
        add_image_size('profPortrait', 480, 650, true); 
        add_image_size('pageBanner', 1500, 350, true); 
    }

    add_action('after_setup_theme', 'theme_actions'); // tema yüklendikten sonra çalıştır.

    function university_post_types()
    {
        // Campus post type
        register_post_type('Campus', array(
            'show_in_rest' => true, // yeni custom post tipi için yeni editörü kullan ve restapiden erişimini sağla
            'supports' => array('title','editor','excerpt'), // Buradaki excerpt elle özet girebilmen için gerekli, 
                                // title ve editor de orada olmak zorunda, yazmayı atlama...
            'rewrite'=> array('slug' => 'campuses'),
            'has_archive' => true,
            'public' => true,
            'labels' => array(
                'name' => "Campus",
                'add_new_item' => 'Add New Campus',
                'edit_item' => "Edit Campus",
                'all_items' => "All Campuses",
                'singular_name' => "Campus",
            ),
            'menu_icon' => "dashicons-location-alt"
        ));
        // event post type
        register_post_type('event', array(
            'show_in_rest' => true, // restapiden erişimini sağla
            'supports' => array('title','editor','excerpt'), // Buradaki excerpt elle özet girebilmen için gerekli, 
                                // title ve editor de orada olmak zorunda, yazmayı atlama...
            'rewrite'=> array('slug' => 'events'),
            'has_archive' => true,
            'public' => true,
            'labels' => array(
                'name' => "Events",
                'add_new_item' => 'Add New Event',
                'edit_item' => "Edit Event",
                'all_items' => "All Events",
                'singular_name' => "Event",
            ),
            'menu_icon' => "dashicons-calendar"
        ));

        // program post type
        register_post_type('program', array(
            'show_in_rest' => true, //restapiden erişimini sağla
            'supports' => array('title','editor'), 
            'rewrite'=> array('slug' => 'programs'),
            'has_archive' => true,
            'public' => true,
            'labels' => array(
                'name' => "Programs",
                'add_new_item' => 'Add New Program',
                'edit_item' => "Edit Program",
                'all_items' => "All Programs",
                'singular_name' => "Program",
            ),
            'menu_icon' => "dashicons-awards"
        ));

        // professor post type
        register_post_type('professor', array(
            'show_in_rest' => true, // restapiden erişimini sağla
            'supports' => array('title','editor','thumbnail'), // thumbnail featured images desteği için gerekli.
            'public' => true,
            'labels' => array(
                'name' => "Professors",
                'add_new_item' => 'Add New Professor',
                'edit_item' => "Edit Professor",
                'all_items' => "All Professors",
                'singular_name' => "Professor",
            ),
            'menu_icon' => "dashicons-welcome-learn-more"
        ));

        // note post type
        register_post_type('note', array(
            'capability_type' => 'note', // by default custom post types inherit their permissions from posts post type
            // to override it we make up new type and we are going to give it new permissions
            'map_meta_cap' => true, // bu satır sayesinde note post tipi için dashboard üzerinden gerekli izinler verilebilecek.
            'show_in_rest' => true, // restapiden erişimini sağla
            'supports' => array('title','editor'),
            'public' => false,
            'show_ui' => true, // public false yapınca default olarak admin dashboardda göstermediği için bu satır eklendi
            'labels' => array(
                'name' => "Notes",
                'add_new_item' => 'Add New Note',
                'edit_item' => "Edit Note",
                'all_items' => "All Notes",
                'singular_name' => "Note",
            ),
            'menu_icon' => "dashicons-welcome-write-blog"
        ));      
        
        // note post type
        register_post_type('like', array(
            'supports' => array('title'),
            'public' => false,
            'show_ui' => true, // public false yapınca default olarak admin dashboardda göstermediği için bu satır eklendi
            'labels' => array(
                'name' => "Likes",
                'add_new_item' => 'Add New Like',
                'edit_item' => "Edit Like",
                'all_items' => "All Likes",
                'singular_name' => "Like",
            ),
            'menu_icon' => "dashicons-heart"
        ));     
        
        // slide post type
        register_post_type('slide', array(
            'supports' => array('title'),
            'public' => false,
            'show_ui' => true, // public false yapınca default olarak admin dashboardda göstermediği için bu satır eklendi
            'labels' => array(
                'name' => "Slides",
                'add_new_item' => 'Add New Slide',
                'edit_item' => "Edit Slide",
                'all_items' => "All Slides",
                'singular_name' => "Slide",
            ),
            'menu_icon' => "dashicons-slides"
        ));         
}

    add_action('init', 'university_post_types');

    function adjust_main_query($query) {
        
        if (!is_admin() AND $query->is_main_query()) { // admin sayfasında değilsek ve main query çalışıyorsa

            if (is_post_type_archive('event')) { // Event post tipi arşiv sayfasındaysak...
                $today = date('Ymd');
                $query->set('meta_key','event_date');
                $query->set('order_by','meta_value_num');
                $query->set('order','ASC');
                $query->set('meta_query', array(
                    array (
                        'key' => 'event_date', // give us the posts which the event_date field values
                        'compare' => '>=', // are equal or greater than
                        'value' => $today, // todays date!,
                        'type' => 'numeric'
                    )
                ));
            }

            if (is_post_type_archive('program')) {
                $query->set('posts_per_page',-1);
                $query->set('orderby',"title");
                $query->set('order',"ASC");
            }

        }
    }

    add_action('pre_get_posts', 'adjust_main_query'); // 

    // Redirect regular subscribers to homepage instead of admin page
    add_action('admin_init', 'redirectSubsToFrontend');

    function redirectSubsToFrontend() {
        $currUser = wp_get_current_user();
        // gelen user sadece bir abone ise
        if (count($currUser->roles) == 1 AND $currUser->roles[0]=="subscriber") {
            wp_redirect(site_url("/")); // onu ana sayfa yönlendir...
            exit;
        }
    }

    // Don't show admin bar to subscribers
    add_action('wp_loaded', 'hideAdminBar');

    function hideAdminBar() {
        $currUser = wp_get_current_user();
        // gelen user sadece bir abone ise
        if (count($currUser->roles) == 1 AND $currUser->roles[0]=="subscriber") {
            show_admin_bar(false);
        }
    }

    // Force note posts to be private, no matter what
    // Çünkü birisi girip oradaki javascript kodunu değiştirebilir.

    // Nadir: Güvenlik açısından çok ama çok önemli...  
    // post edilen veri yazılmadan önce bu filtreyi kullan 
    add_filter("wp_insert_post_data", 'makeNotePrivate', 10, 2); // 10 : bu hook için öncelik sırası, 2 : fonksiyona kaç adet parametre gönderilecek

    function makeNotePrivate($data, $postInfo) { // $data, ön yüzden gelen veri, $postInfo post ile ilgili bilgi
        
        // post request durdurulabiliyor mu?
        // php kodu nasıl debug edilebilir?

        if ($data['post_type'] == 'note') {
            // textareadan gelen içerikte herhangi bir html tagi olmasına izin verme!
            $data['post_content'] = sanitize_textarea_field($data['post_content']);
            $data['post_title'] = sanitize_text_field($data['post_title']);

            // !$postInfo["ID"] => Ancak yeni bir post oluştururken ID olmaz, böylece güncelleme ve silme işleminde 
            // buradaki kodu çalıştırmamış oluruz.
            if (count_user_posts(get_current_user_id(), "note") > 4 AND !$postInfo["ID"]) {
                die("You have reached your personal note limit!");
                // Bu satırdan sonrası çalışmayacak...
            }
        }

        if ($data['post_type'] == 'note' AND $data['post_status']!="trash") {
            $data['post_status'] = "private";
        }


        return $data; // filtreden geçtikten sonra çıkan veri
    }

?>