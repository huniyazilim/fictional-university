<?php 
    get_header();
    while(have_posts()) { 
        the_post();
        pageBanner();
?>

<div class="container container--narrow page-section">
    <div class="metabox metabox--position-up metabox--with-home-link">
        <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('event'); ?>">
            <i class="fa fa-home" aria-hidden="true"></i> Events </a> 
            <span class="metabox__main">                
                <?php the_title(); ?>
            </span>
        </p>
        </div>     
    <div class="generic-content">
        <?php the_content() ?>
    </div>

    <?php 
        $relatedPrograms = get_field('related_programs'); 
        // Relation custom type, bağlı bulunulan post tipinde bir array döner.
        // Burada eventlerle bağlantılı programlar vardır ve get_field('related_programs')
        // ilgili evente bağlı bulunan program post objelerinin bir listesini dizi olarak döner.
        // Ve get_the_title(), get_the_permalink() gibi fonksiyonlar argüman olarak id yerine doğrudan post objesini de kullabilir...
        
        // print_r($relatedPrograms); // writes object to page
        // echo json_encode($relatedPrograms[0]);  
        // $obj = json_decode(jsonString);

        if ($relatedPrograms) {
            echo '<hr class="section-break"></hr>';
            echo '<h2 class="headline headline--medium">Related Program(s)</h2>';
            echo "<ul class='min-list link-list'>";
            foreach ($relatedPrograms as $program) { // returns a program typed post object
    ?>
        <li>
            <a href="<?php echo get_the_permalink($program); ?>"><?php echo get_the_title($program); ?></a>
        </li>
    <?php }
        echo "</ul>";
    }
    ?>
</div>

<?php } // end while
    get_footer();
?>
