<?php 
    get_header();
    while(have_posts()) { 
        the_post();
        pageBanner();
?>

<div class="container container--narrow page-section">
    
    <div class="generic-content">
        <h4><?php echo get_field("page_banner_subtitle"); ?></h4>
        <?php the_content() ?>
    </div>

</div>

<?php } // end while
    get_footer();
?>
