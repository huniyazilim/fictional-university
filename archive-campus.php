<?php get_header();
    pageBanner(array(
        'title' => "Our Campuses",
        'subtitle' =>"Lots of campuses spread across."
    ));
?>

<div class="container container--narrow page-section">
    <ul class="link-list min-list">
    <?php 
        while (have_posts()) {
            the_post(); 
        ?>

        <li>
            <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
            <p><?php echo wp_trim_words(get_the_content(), 18); ?>  
            <a href="<?php the_permalink(); ?>" class="nu gray">Learn more</a></p>
        </li>

    <?php } // end while ?>
    </ul>

    <?php echo paginate_links(); ?>
  
</div>

<?php get_footer();?>
