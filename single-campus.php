<link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/css/ol.css" type="text/css">
<script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.5.0/build/ol.js"></script>
<style>
      .ol-map {
        height: 400px;
        width: 100%;
        background-color : gainsboro;
        margin-top : 20px;
      }
</style>
<?php 
    get_header();
    while(have_posts()) { 
        the_post();
        pageBanner();
        $mapLocation = explode(",", get_field("map_location"));
    ?>

    <div class="container container--narrow page-section">

        <div class="metabox metabox--position-up metabox--with-home-link">
            <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('campus'); ?>">
                <i class="fa fa-home" aria-hidden="true"></i> Tüm Kampüsler </a> 
                <span class="metabox__main">                
                    <?php the_title(); ?>
                </span>
            </p>
        </div>     

        <div class="generic-content">
            <?php the_content(); ?>
        </div>
        <div class="ol-map" id="ol-map" title="Kampüs haritası"></div>
    
        <script>
            var map = new ol.Map({
                target: 'ol-map',
                layers: [
                    new ol.layer.Tile({
                            source: new ol.source.OSM()
                        })
                ],
                view: new ol.View({
                    // googleMaps harita bilgis : lat, lon, zoom =>  39.8967524,32.7823573,14z
                    center: ol.proj.fromLonLat([ <?php echo (float)$mapLocation[1]; ?>, <?php echo (float)$mapLocation[0]; ?>]),
                    zoom: <?php echo (int)$mapLocation[2]; ?>
                    // Sondaki z harfinden inte çevirerek kurtul :)
                })
            });
        </script>    
    
<?php } // end while
    get_footer();
?>
