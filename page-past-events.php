<?php get_header();
    pageBanner(array(
        'title' => 'Past Events',
        'subtitle' => 'Geçmişte neler yapmışız, neler!'
    ));
?>

<div class="container container--narrow page-section">
    <?php 
        $today = date('Ymd'); // bu format custom event_date alanın döndürdüğü format ile aynı!
        $pastEvents = new WP_Query(array(
            // paged => Sayfalama hangi sayfayı gösterecek onu söyle bana
            'paged' => get_query_var('paged', 1), // get_query_var ile bu değeri adresten al
            'post_type' => "event",
            // 'order_by' => 'title', // 'post_date' is the default, 'rand' yazarsan rastgele sıralama yapar :)
            'order' => 'ASC', // DESC is the default
            'meta_key' => 'event_date', // Hangi custom fielda göre sıralama yapacağız
            'order_by' => 'meta_value_num', 
            'meta_query' => array( // Burada birden fazla array ekleyerek çoklu koşul belirtebilirsin.
                array (
                    'key' => 'event_date', // give us the posts which the event_date field values
                    'compare' => '<', // are less than
                    'value' => $today, // todays date!,
                    'type' => 'numeric'
                )
            )     
        ));


        while ($pastEvents->have_posts()) {
            $pastEvents->the_post(); 
            get_template_part('template-parts/content', 'event');           
        } 
        wp_reset_postdata();
    ?>
    <?php echo paginate_links(array( // Custom query söz konusu olduğunda pagination ile ilgili verileri bizim söylememiz gerekiyor...
        'total' => $pastEvents->max_num_pages, // Toplam sayfa sayısını söyle
    )); ?>
</div>

<?php get_footer();?>
