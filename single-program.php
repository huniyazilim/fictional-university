<?php 
    get_header();
    while(have_posts()) { 
        the_post();
        pageBanner();
?>

<div class="container container--narrow page-section">
    <div class="metabox metabox--position-up metabox--with-home-link">
        <p><a class="metabox__blog-home-link" href="<?php echo get_post_type_archive_link('program'); ?>">
            <i class="fa fa-home" aria-hidden="true"></i> All Programs </a> 
            <span class="metabox__main">                
                <?php the_title(); ?>
            </span>
        </p>
        </div>     
    <div class="generic-content">
        <?php the_content(); ?>
    </div>

    <?php 
        $today = date('Ymd'); // bu format custom event_date alanın döndürdüğü format ile aynı!
        $events = new WP_Query(array(
            'post_type' => "event",
            'posts_per_page' => 2, // sayfa başına sadece iki kayıt getir.
            // 'posts_per_page' => -1, // Bana bu sorgu koşullarına uyan tüm kayıtları getir!
            // 'order_by' => 'title', // 'post_date' is the default, 'rand' yazarsan rastgele sıralama yapar :)
            'order' => 'ASC', // DESC is the default
            'meta_key' => 'event_date', // Hangi custom fielda göre sıralama yapacağız
            'order_by' => 'meta_value_num', 
            'meta_query' => array( // Burada birden fazla array ekleyerek çoklu koşul belirtebilirsin.
                array (
                    'key' => 'event_date', // give us the posts which the event_date field values
                    'compare' => '>=', // are equal or greater than
                    'value' => $today, // todays date!,
                    'type' => 'numeric'
                ),
                array ( // AND
                    'key' => 'related_programs', 
                    'compare' => 'LIKE', 
                    'value' => '"' . get_the_ID() . '"'  
                    // mySql bir diziyi string olarak serialize ettiği için bu şekilde arıyoruz.
                ),                        

            )
        ));            
           
        $relatedProfs = new WP_Query(array(
            'post_type' => "professor",
            'posts_per_page' => -1, // Bana bu sorgu koşullarına uyan tüm kayıtları getir!
            'order_by' => 'title', 
            'order' => 'ASC',
            'meta_query' => array( 
                array ( 
                    'key' => 'related_programs', 
                    'compare' => 'LIKE', 
                    'value' => '"' . get_the_ID() . '"'  
                    // mySql bir diziyi string olarak serialize ettiği için bu şekilde arıyoruz.
                ),                        
            )
        ));                

        $hasProfs = $relatedProfs->have_posts();

        if ($hasProfs) {
            echo '<hr class="section-break">';
            echo '<h2 class="headline headline--medium">' . get_the_title() . ' Professors </h2>';
            echo '<ul class="professor-cards">';
        }

        while($relatedProfs->have_posts()) {
            $relatedProfs->the_post(); ?>
            <li class="professor-card__list-item">
                <a class="professor-card" href="<?php the_permalink(); ?>">
                <img class="professor-card__image" src="<?php the_post_thumbnail_url('profLandscape'); ?>">
                <span class="professor-card__name"><?php the_title(); ?></span>
            </a>
            </li>
        <?php } // end while relatedProfs

        if ($hasProfs) echo '</ul>';
        wp_reset_postdata();

        if ($events->have_posts() ) {
            echo '<hr class="section-break">';
            echo '<h2 class="headline headline--medium">Upcoming ' . get_the_title() . ' Events </h2>';
        }

        while($events->have_posts()) {
            $events->the_post(); 
            get_template_part('template-parts/content', 'event');                          
        } 
        wp_reset_postdata();
    ?>

</div>

<?php } // end while
    get_footer();
?>
