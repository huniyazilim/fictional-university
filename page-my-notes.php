<?php 

    if (!is_user_logged_in()){
        wp_redirect(esc_url(site_url("/"))); // onu ana sayfa yönlendir...
        exit; // don't run this php code after redirect, bc no need to
    }

    // Exit etmeden önce buradaki kodun devamını çalışmak mümkün mü? araştır

    get_header();
    pageBanner(array(
        'title' => 'Not Alın',
        'subtitle' => 'Tüm ders notlarınız burada!'
    ));
    while(have_posts()) { 
        the_post();
    ?>

    <div class="container container--narrow page-section">

        <div class="create-note">
            <h2 class="headline headline--medium">Yeni not ekle</h2>
            <input class="new-note-title" placeholder="Not başlığı"/>
            <textarea class="new-note-body"></textarea>
            <span class="submit-note" data-action="create">Ekle</span>
        </div>

        <ul class="min-list link-list" id="my-notes">
            <?php 

                $userNotes = new WP_Query(array(
                    'posts_per_page' => -1,
                    'post_type' => 'note',
                    'author' => get_current_user_id()
                ));

                while($userNotes->have_posts()) {
                    $userNotes->the_post();
                    $noteId = $userNotes->post->ID;
            ?>
                <li>
                    <input readonly class="note-title-field" 
                        value="<?php echo stripPrivate(esc_attr(get_the_title())); ?>"/>
                    <span class="edit-note" data-note-id="<?php echo $noteId; ?>" data-action="edit">
                        <i style="pointer-events:none; margin-right:0.5em" class="fa fa-pencil" aria-hidden="true"></i>Düzenle</span>
                    <span class="delete-note" data-note-id="<?php echo $noteId; ?>" data-action="delete">
                        <i style="pointer-events:none; margin-right:0.5em" class="fa fa-trash-o" aria-hidden="true"></i>Sil</span>
                    <textarea readonly class="note-body-field">
                        <?php echo esc_attr(wp_strip_all_tags(get_the_content())); ?>
                    </textarea>
                    <span class="update-note btn btn--blue btn--small" data-note-id="<?php echo $noteId; ?>" data-action="save">
                        <i style="pointer-events:none; margin-right:0.5em" class="fa fa-arrow-right" aria-hidden="true"></i>Kaydet</span>
                </li>
            <?php        
                } // end while
            ?>
        </ul>
    </div>
        
<?php } 
    get_footer();
?>
