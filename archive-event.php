<?php 
    get_header();
    // $result = $initial ?: 'default';
    pageBanner(array(
        'title' => "All Events",
        'subtitle' =>"See what will be happenening."
    ));

?>

<div class="container container--narrow page-section">
    <?php 
        while (have_posts()) {
            the_post(); 
            get_template_part('template-parts/content-event');    
            } // end while 
    ?>
    <?php echo paginate_links(); ?>

    <hr class="section-break">

    <p class="t-center">
        <a href="<?php echo esc_url(site_url("/past-events")); ?>">
            View Past Events From Here
        </a>
    </p>

    
</div>

<?php get_footer();?>
