<?php 
    get_header();
    while(have_posts()) { 
        the_post();
        pageBanner();
?>

<div class="container container--narrow page-section">
    <div class="metabox metabox--position-up metabox--with-home-link">
        <p><a class="metabox__blog-home-link" href="<?php echo esc_url(site_url("/posts")); ?>">
            <i class="fa fa-home" aria-hidden="true"></i> Blog Posts </a> 
            <span class="metabox__main">                
                <?php the_author_posts_link() ?> tarafından 
                <?php the_time('j.n.y') ?> tarihinde 
                <?php echo get_the_category_list(", ") ?> altında yayınlanmıştır. 
            </span>
        </p>
        </div>     
    <div class="generic-content">
        <?php the_content() ?>
    </div>
</div>

<?php } // end while
    get_footer();
?>
