<!-- <h2 style="color:green">page.php page is being shown, this page is used to show a pages!</h2> -->

<?php 
    get_header();
    while(have_posts()) { the_post();
?>

    <div class="page-banner">
        <div class="page-banner__bg-image" style="background-image: url(<?php echo get_theme_file_uri('/images/ocean.jpg'); ?>"></div>
            <div class="page-banner__content container container--narrow">
                <h1 class="page-banner__title"><?php the_title() ?></h1>
                <div class="page-banner__intro">
                    <p>Learn how the school of your dreams got started.</p>
                </div>
            </div>  
        </div>
    </div>

    <div class="container container--narrow page-section">

        <?php     
            // eğer parentID 0 dışında birşey ise, yani bu bir child page ise
            // eğer parentID == 0 ise bir parent page demektir.

            $parentID = wp_get_post_parent_id(get_the_ID());
            $parentTitle = get_the_title($parentID);
            $permaLink = get_the_permalink($parentID);

            if ($parentID != 0) {
        ?>

            <div class="metabox metabox--position-up metabox--with-home-link">
            <p><a class="metabox__blog-home-link" href="<?php echo $permaLink ?>">
                <i class="fa fa-home" aria-hidden="true"></i> Back to <?php echo $parentTitle ?></a> 
                <span class="metabox__main"><?php the_title() ?></span></p>
            </div>            

        <?php  } // end if ?>    
    

        <?php 

            $hasChildren = get_pages(array(
                "child_of" => get_the_ID()
            )); // eğer get_pages'den dizi dönerse çocukları var demektir.
        
            if ($parentID!=0 or $hasChildren) { // Bir cocuk sayfasında isek ve ilgili sayfanın alt sayfaları varsa.

        ?>

            <div class="page-links">
            <h2 class="page-links__title"><a href="<?php echo $permaLink ?>"><?php echo $parentTitle ?></a></h2>
            <ul class="min-list">
                    <?php 
                        
                        wp_list_pages(array(
                            'title_li' => NULL, // do not show the title,
                            'child_of' => ($parentID != 0) ? $parentID : get_the_ID() // parent page ise yine de çocuklarını göster.
                        )) 
                    ?>
            </ul>
            </div>

        <?php } // end if ?>

        <div class="generic-content">
        <?php the_content() ?> 
        </div>

    </div>

<?php } 
    get_footer();
?>
