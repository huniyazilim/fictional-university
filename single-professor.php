<?php 
    get_header();
    while(have_posts()) { 
        the_post();
        pageBanner();

        $likeCount = new WP_Query(array(
            'post_type' => "like",
            'meta_query'=> array(
                array(
                    'key' => 'liked_professor_id',
                    'compare' => "=", // equals
                    'value' => get_the_ID() // gives the current professors id
                )
            )
        ));

        function getLikeId() {

            $query = new WP_Query(array(
                'author' => get_current_user_id(),
                'post_type' => "like",
                'meta_query'=> array(
                    array(
                        'key' => 'liked_professor_id',
                        'compare' => "=", // equals
                        'value' => get_the_ID() // gives the current professors id
                    )
                )
            ));

            return $query->found_posts > 0 ? $query->posts[0]->ID : NULL;
        }

        function isLikedByThisAuthor() {
            if (!is_user_logged_in()) return "no";
            return getLikeId() != NULL ? "yes" : "no"; 
        }        

    ?>

<div class="container container--narrow page-section">
    
    <div class="generic-content">
        <div class="row group">
            <div class="one-third"><?php the_post_thumbnail('profPortrait'); ?></div>
            <div class="two-thirds">
                <span class="like-box" 
                    data-exists="<?php echo isLikedByThisAuthor(); ?>"
                    data-professor-id="<?php the_ID(); ?>"
                    data-like-id="<?php echo getLikeId(); ?>"
                >
                    <i style="pointer-events:none;" class="fa fa-heart-o" aria-hidden="true"></i>
                    <i style="pointer-events:none;" class="fa fa-heart" aria-hidden="true"></i>
                    <span id = "like-count" style="pointer-events:none;" class="like-count"><?php echo $likeCount->found_posts; ?></span>
                </span>
                <?php the_content() ?>
            </div>
        </div>
    </div>

    <?php 
        $relatedPrograms = get_field('related_programs'); 
        // Relation custom type, bağlı bulunulan post tipinde bir array döner.
        // Burada eventlerle bağlantılı programlar vardır ve get_field('related_programs')
        // ilgili evente bağlı bulunan program post objelerinin bir listesini dizi olarak döner.
        // Ve get_the_title(), get_the_permalink() gibi fonksiyonlar argüman olarak id yerine doğrudan post objesini de kullabilir...
        
        // print_r($relatedPrograms); // writes object to page
        // echo json_encode($relatedPrograms[0]);  
        // $obj = json_decode(jsonString);

        if ($relatedPrograms) {
            echo '<hr class="section-break"></hr>';
            echo '<h2 class="headline headline--medium">Subject(s) Thought</h2>';
            echo "<ul class='min-list link-list'>";
            foreach ($relatedPrograms as $program) { // returns a program typed post object
    ?>
        <li>
            <a href="<?php echo get_the_permalink($program); ?>"><?php echo get_the_title($program); ?></a>
        </li>
    <?php }
        echo "</ul>";
    }
    ?>
</div>

<?php } // end while
    get_footer();
?>
