(function($) {

    // function.php dosyası içinde wp_localize_script ile sağolsun bu bilgiyi 
    // wp sayfanın içine js var olarak gömüyor.
    const rootUrl = themeData.root_url;

    /* ===============================================================
      Mobile Menu Feature
    =============================================================== */
    class MobileMenu {
      constructor() {
        this.menu = document.querySelector(".site-header__menu")
        this.openButton = document.querySelector(".site-header__menu-trigger")
        this.events()
      }
    
      events() {
        this.openButton.addEventListener("click", () => this.openMenu())
      }
    
      openMenu() {
        this.openButton.classList.toggle("fa-bars")
        this.openButton.classList.toggle("fa-window-close")
        this.menu.classList.toggle("site-header__menu--active")
      }
    }
    
    new MobileMenu()
    
    /* ===============================================================
      Hero Slider / Homepage Slideshow
    =============================================================== */
    class HeroSlider {
      constructor() {
        if (document.querySelector(".hero-slider")) {
          // count how many slides there are
          const dotCount = document.querySelectorAll(".hero-slider__slide").length
    
          // Generate the HTML for the navigation dots
          let dotHTML = ""
          for (let i = 0; i < dotCount; i++) {
            dotHTML += `<button class="slider__bullet glide__bullet" data-glide-dir="=${i}"></button>`
          }
    
          // Add the dots HTML to the DOM
          document.querySelector(".glide__bullets").insertAdjacentHTML("beforeend", dotHTML)
    
          // Actually initialize the glide / slider script
          var glide = new Glide(".hero-slider", {
            type: "carousel",
            perView: 1,
            autoplay: 3000
          })
    
          glide.mount()
        }
      }
    }
    
    new HeroSlider()
    
    /* ===============================================================
      Google Map / Campus Map
    =============================================================== */
    class GoogleMap {
      constructor() {
        document.querySelectorAll(".acf-map").forEach(el => {
          this.new_map(el)
        })
      }
    
      new_map($el) {
        var $markers = $el.querySelectorAll(".marker")
    
        var args = {
          zoom: 16,
          center: new google.maps.LatLng(0, 0),
          mapTypeId: google.maps.MapTypeId.ROADMAP
        }
    
        var map = new google.maps.Map($el, args)
        map.markers = []
        var that = this
    
        // add markers
        $markers.forEach(function (x) {
          that.add_marker(x, map)
        })
    
        // center map
        this.center_map(map)
      } // end new_map
    
      add_marker($marker, map) {
        var latlng = new google.maps.LatLng($marker.getAttribute("data-lat"), $marker.getAttribute("data-lng"))
    
        var marker = new google.maps.Marker({
          position: latlng,
          map: map
        })
    
        map.markers.push(marker)
    
        // if marker contains HTML, add it to an infoWindow
        if ($marker.innerHTML) {
          // create info window
          var infowindow = new google.maps.InfoWindow({
            content: $marker.innerHTML
          })
    
          // show info window when marker is clicked
          google.maps.event.addListener(marker, "click", function () {
            infowindow.open(map, marker)
          })
        }
      } // end add_marker
    
      center_map(map) {
        var bounds = new google.maps.LatLngBounds()
    
        // loop through all markers and create bounds
        map.markers.forEach(function (marker) {
          var latlng = new google.maps.LatLng(marker.position.lat(), marker.position.lng())
    
          bounds.extend(latlng)
        })
    
        // only 1 marker?
        if (map.markers.length == 1) {
          // set center of map
          map.setCenter(bounds.getCenter())
          map.setZoom(16)
        } else {
          // fit to bounds
          map.fitBounds(bounds)
        }
      } // end center_map
    }
    
    new GoogleMap()
    
    /* ===============================================================
        Search / Live Overlay Results
    =============================================================== */
    class Search {
        // 1. initiate
        constructor() {
            //mobil ve normal görünüm için birden fazla aynı adlı arama ikonu olduğu için.
            this.searchIcons = document.querySelectorAll(".js-search-trigger");
            this.closeButton = document.querySelector(".search-overlay__close");
            this.searchOverlay = document.querySelector(".search-overlay");
            this.searchInput = document.getElementById("search-term");
            this.resultsDiv = document.getElementById("search-overlay__results");
            this.isSearhOverlayOpen = false;
            this.typingTimer = undefined;
            this.previousSearchTerm="";
            this.relatedTypes = undefined;
            this.bindEvents();
            this.rootUrl = themeData.root_url;
        }
        // 2. events
        bindEvents() {
            this.searchIcons.forEach(icon => icon.addEventListener("click",this.openOverlay.bind(this)));
            this.closeButton.addEventListener("click", this.closeOverlay.bind(this));
            document.addEventListener("keyup", this.keyPressDispatcher.bind(this));
            this.searchInput.addEventListener("keyup", this.handleSearchTextInput.bind(this));
        }
        
        keyPressDispatcher(e) {
            // key olayı body dışında bir input veya textarea'dan da gelebilir, o zaman göz ardı et.
            if (e.target.tagName.toUpperCase()!=="BODY") return;
            switch (e.keyCode) {
                case 83:
                    if (!this.isSearhOverlayOpen) {
                        this.openOverlay();
                    }
                    break;
                case 27:
                    if (this.isSearhOverlayOpen) {
                        this.closeOverlay();
                    }
                    break; 
                default:
                    break;
            }
        }

        // 3. methods
        openOverlay(e) {
            e.preventDefault(); // bunlar a taglari ve fallback olarak /search'e yönlendirilmiş durumdalar
            // dolayısıyla bunun engellenmesi gerekiyor.

            // Burada overlay açıldığında ilişkili tip verisi cache'e alınıyor. Bu bir egzersiz olarak yapıldı.
            // Gerekirse arama verileri ile daha sonra önyüzde birleştirilebilir. Şimdilik uğraşmayacağım...
            this.getRelatedTypesData(); // arama sözcüğü yazılana kadar veriler arkadan getirilebilir.
            this.searchOverlay.classList.add("search-overlay--active");
            setTimeout(() => {
                this.searchInput.focus();
                this.searchInput.value="";
                this.isSearhOverlayOpen = true;
            }, 50);
            document.body.classList.add("body-no-scroll");
            this.resultsDiv.innerHTML = "";
        }

        closeOverlay() {
            this.searchOverlay.classList.remove("search-overlay--active");
            document.body.classList.remove("body-no-scroll");
            this.isSearhOverlayOpen = false;
        }

        handleSearchTextInput(e) {
            if (e.keyCode == 13) {
                clearTimeout(this.typingTimer);
                this.getCustomSearchResults(e);
                return;
            }
            this.searchInput.value = this.searchInput.value.trim();
            this.previousSearchTerm = this.searchInput.value;
            clearTimeout(this.typingTimer);
            this.typingTimer = setTimeout(this.getCustomSearchResults.bind(this, e), 750); // Use custom api
            // this.typingTimer = setTimeout(this.getSearchResults.bind(this, e), 750); // Use default wp-json api
        }

        // Use custom api
        async getCustomSearchResults(e) {
            console.log("getCustomSearchResults");
            const searchTerm = e.target.value.trim();
            if (searchTerm == "" || searchTerm.length<3) return;
            this.resultsDiv.innerHTML = '<div class="spinner-loader"></div>';

            try {
                $.getJSON(this.rootUrl + '/wp-json/api/v1/search?term='+searchTerm)
                .then(allResults => {
                    if (allResults.length == 0) {
                        this.resultsDiv.innerHTML=`
                        <h2 class="search-search-overlay__section-title">Arama sonuçları</h2>
                        <ul class="link-list min-list">
                            <p>Sonuç bulunamadı.</p>
                        </ul>
                        `
                    } else {
                        console.log(allResults);
                        this.resultsDiv.innerHTML=`
                                <h2 class="search-search-overlay__section-title">Arama sonuçları</h2>
                                <ul class="link-list min-list">
                                    ${allResults.map(item => `<li><a href="${item.permalink}">${item.title}</li></a>`).join('')}
                                </ul>
                            ` 
                    }
                });
            } catch (error) {
                this.resultsDiv.innerHTML=`
                <h2 class="search-search-overlay__section-title">Arama sonuçları</h2>
                <ul class="link-list min-list">
                    <p>Arama sırasında bir hata oluştu :(</p>
                </ul>
                `
            }
    
        }

        // Use default wp-json api
        async getSearchResults(e) {
            const searchTerm = e.target.value.trim();
            if (searchTerm == "" || searchTerm.length<3) return;
            this.resultsDiv.innerHTML = '<div class="spinner-loader"></div>';
            
            // function.php dosyası içinde wp_localize_script ile sağolsun bu bilgiyi 
            // wp sayfanın içine js var olarak gömüyor.
            const rootUrl = themeData.root_url;
            const searchTypes = ["posts", "pages", "campus", "professor"];

            const requests = searchTypes.map(type => {
                return $.getJSON(rootUrl + '/wp-json/wp/v2/' + type + '?search=' + searchTerm);
            }); 

            let allResults=[];

            try {
                // tüm promislerden dönen sonuçları, ki onlar da birer dizi, tek bir dizide topla...
                allResults = (await Promise.all(requests)).reduce((result, cur) => result.concat(cur), []);    
            } catch (error) {
                this.resultsDiv.innerHTML=`
                <h2 class="search-search-overlay__section-title">Arama sonuçları</h2>
                <ul class="link-list min-list">
                    <p>Arama sırasında bir hata oluştu :(</p>
                </ul>
                `
            }

            if (allResults.length == 0) {
                this.resultsDiv.innerHTML=`
                <h2 class="search-search-overlay__section-title">Arama sonuçları</h2>
                <ul class="link-list min-list">
                    <p>Sonuç bulunamadı.</p>
                </ul>
                `
            } else {
                this.resultsDiv.innerHTML=`
                        <h2 class="search-search-overlay__section-title">Arama sonuçları</h2>
                        <ul class="link-list min-list">
                            ${allResults.map(item => `<li><a href="${item.link}">${item.title.rendered}</li></a>`).join('')}
                        </ul>
                    ` 
            }

            console.log(allResults);

        }

        async getRelatedTypesData() {
            if (this.relatedTypes) return this.relatedTypes;
            await $.getJSON(this.rootUrl + '/wp-json/api/v1/getRelatedTypesData')
                .then(results => {
                    this.relatedTypes = results;
                });
            return this.relatedTypes;
        }

    }

    const liveSearch = new Search();
    
    /* ===============================================================
        My Notes Feature
    =============================================================== */
    class MyNotes {
        constructor(){
            console.log("MyNotes is in the game.");
            this.bindEvents();
        }

        bindEvents() {

            if (!document.getElementById("my-notes")) return;

            document.getElementById("my-notes")
                .addEventListener("click", (e) => {
                    if (e.target.tagName.toUpperCase() != "SPAN") return false;
                    console.log(e.target.dataset);
                    const {noteId, action} = e.target.dataset;
                    switch (action) {
                        case "delete":
                            this.deleteNote(noteId, e);
                            break;
                        case "edit":
                            this.makeNoteEditable(e);
                            break;     
                        case "save":
                            this.saveNote(noteId, e);
                            break;                                            
                        default:
                            break;
                    }
                }
            );

            document.querySelector(".submit-note")
                .addEventListener("click", (e) => {
                    this.createNote(e);
            });
        }

        deleteNote(noteId, e) {

            console.log("Trying to delete note : " + noteId);
            const currentNote = e.target.parentNode;
            $.ajax({
                beforeSend : (xhr) => {
                    // wp rest api otantikasyonu için gerekli
                    xhr.setRequestHeader("X-WP-Nonce", themeData.nonce);
                },
                url : rootUrl + "/wp-json/wp/v2/note/" + noteId,
                type : "DELETE",
                success : (response) => {
                    console.log("Silme işlemi başarılı");
                    // console.log(response);
                    $(currentNote).slideUp(500, () => { // animation duration 500ms
                        $(currentNote).remove();
                    });
                },
                error : (response) => {
                    alert("Note silinirken bir hata oluştur :(");
                    console.log(response);
                }
            });
        }

        makeNoteEditable(e) {

            const currentNote = e.target.parentNode;
            const fields = currentNote.querySelectorAll(".note-title-field, .note-body-field");
            const saveButton = currentNote.querySelector(".update-note");               

            fields.forEach(el => {
                el.classList.toggle("note-active-field")
                el.removeAttribute("readonly");
            });
            saveButton.classList.toggle("update-note--visible");

        }        

        saveNote(noteId, e) {
            console.log("Trying to edit note : " + noteId);
            const currentNote = e.target.parentNode;
            const fields = currentNote.querySelectorAll(".note-title-field, .note-body-field");
            const saveButton = currentNote.querySelector(".update-note");               

            const updatedNote = {
                'title' : currentNote.querySelector(".note-title-field").value,
                'content' : currentNote.querySelector(".note-body-field").value,
            }

            $.ajax({
                beforeSend : (xhr) => {
                    // wp rest api otantikasyonu için gerekli
                    xhr.setRequestHeader("X-WP-Nonce", themeData.nonce);
                },
                data : updatedNote,
                url : rootUrl + "/wp-json/wp/v2/note/" + noteId,
                type : "POST",
                success : (response) => {
                    console.log("Güncelleme işlemi başarılı");
                    fields.forEach(el => {
                        el.classList.toggle("note-active-field")
                        el.setAttribute("readonly","readonly");
                    });
                    saveButton.classList.toggle("update-note--visible");
                    console.log(response);
                },
                error : (response) => {
                    alert("Not güncellenmeye çalışılırken bir hata oluştu :(");
                    console.log(response);
                }
            });
        }

        createNote(e) {
            console.log("Trying to create new note");
            const currentNote = e.target.parentNode;       
            const titleField = currentNote.querySelector(".new-note-title");
            const bodyField = currentNote.querySelector(".new-note-body");
            const newNote = {
                'title' : titleField.value,
                'content' : bodyField.value,
                'status' : "private", // varsayılanı "trash", "draft", "publish" veya "private" olabilir.
            }

            $.ajax({
                beforeSend : (xhr) => {
                    // wp rest api otantikasyonu için gerekli
                    xhr.setRequestHeader("X-WP-Nonce", themeData.nonce);
                },
                data : newNote,
                url : rootUrl + "/wp-json/wp/v2/note/", // Bu tipten yeni bir tane oluşturmak için id kısmını boş bırak :)
                type : "POST",
                success : (response) => {
                    console.log(response);
                    alert("Yeni not başarıyla eklendi.");
                    titleField.value = "";
                    bodyField.value = "";
                    window.location.reload();
                },
                error : (response) => {
                    if (response.responseText == "You have reached your personal note limit!") {
                        alert("Not kaydetme limitinize ulaştınız, eski notları silmeyi deneyiniz.");
                        console.log(response);
                        return;
                    }
                    alert("Yeni not eklemeye çalışılırken bir hata oluştu :(");
                    console.log(response);
                }
            });
        }        

    }

    new MyNotes();
    
    /* ===============================================================
      Like a Professor Feature
    =============================================================== */
    class Like {
        constructor() {
            console.log("Like api is alive...");
            this.heart = document.querySelector(".like-box");
            this.bindEvents();
        }

        bindEvents() {
            if (this.heart){
                this.heart.addEventListener("click", this.onClickHandler.bind(this));
            }
        }

        onClickHandler(e) {
            if (this.heart) {
                if (this.heart.dataset.exists == "yes") {
                    this.deleteLike(e);
                } else {
                    this.addLike(e);
                }
            }
        }

        addLike(e){

            const likeData = {
                professorId : e.target.dataset.professorId
            }
            $.ajax({
                beforeSend : (xhr) => {
                    // wp rest api otantikasyonu için gerekli
                    xhr.setRequestHeader("X-WP-Nonce", themeData.nonce);
                },
                data : likeData,
                url : rootUrl + "/wp-json/api/v1/manageLike", // Bu tipten yeni bir tane oluşturmak için id kısmını boş bırak :)
                type : "POST",
                success : (response) => {
                    console.log(response);
                    e.target.dataset.exists = "yes";
                    e.target.dataset.likeId = response.likeId;
                    let currentLikeCount = parseInt(document.getElementById("like-count").innerText, 10);
                    currentLikeCount++;
                    document.getElementById("like-count").innerText = currentLikeCount;

                },
                error : (response) => {
                    alert("Beğeni eklemeye çalışılırken bir hata oluştu :(");
                    console.log(response);
                }
            });
        }

        deleteLike(e) { 
            const likeId = e.target.dataset.likeId;
            $.ajax({
                beforeSend : (xhr) => {
                    // wp rest api otantikasyonu için gerekli
                    xhr.setRequestHeader("X-WP-Nonce", themeData.nonce);
                },
                data : {
                    likeId : likeId
                },
                url : rootUrl + "/wp-json/api/v1/manageLike",
                type : "DELETE",
                success : (response) => {
                    let currentLikeCount = parseInt(document.getElementById("like-count").innerText, 10);
                    currentLikeCount--;
                    document.getElementById("like-count").innerText = currentLikeCount;
                    e.target.dataset.exists = "no";
                    e.target.dataset.likeId = "";
                    console.log("Silme işlemi başarılı");
                    console.log(response);
                },
                error : (response) => {
                    alert("Like silinirken bir hata oluştur :(");
                    console.log(response);
                }
            }); 
        }
    }
    
    new Like();
    
})(jQuery); // End self invoking function.