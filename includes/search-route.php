<?php

//Build your own rest api endpoints here! 

add_action('rest_api_init','registerSearch');

function registerSearch()
{
    register_rest_route('api/v1', 'search', array(
        'methods' => WP_REST_SERVER::READABLE, // 'GET' basicly, but there might be some changes depending on server so wp deals with them for us
        'callback' => 'getSearchResults'
    ));
    register_rest_route('api/v1', 'getRelatedTypesData', array(
        'methods' => WP_REST_SERVER::READABLE, // 'GET' basicly, but there might be some changes depending on server so wp deals with them for us
        'callback' => 'getRelatedTypesData'
    ));
}

// Sql kullanarak sorgu çekme örneği...
// to do buraya related program idlerini dizi olarak ver...
function getRelatedPrograms() {
    // $posts = $wpdb->get_results("SELECT ID, post_title FROM $wpdb->posts WHERE post_status = 'publish'
    // AND post_type='post' ORDER BY comment_count DESC LIMIT 0,4");
    global $wpdb;
    $results = $wpdb->get_results("SELECT * FROM $wpdb->posts WHERE post_status = 'publish'
    AND post_type='program'");    
    return $results;
}
 
function getSearchResults($queryParams) {
    $query = new WP_Query(array(
        'posts_per_page' => -1, // tüm sonuçları getir...
        // Bu şekilde birden fazla type içinde query çalıştırabiliyormuşsun harika!!!
        'post_type' => array("post","page","campus","event","professor","campus","program"),
        's' => sanitize_text_field($queryParams["term"]) // s for search
    ));

    $searchReuslts = array();
    while($query->have_posts()){
        $currentRow = $query->the_post();
        array_push($searchReuslts, array( // sonuç dizimizi oluşturuyoruz.
            "id" => $query->post->ID,
            "title" => get_the_title(),
            "permalink" => get_the_permalink(),
            "totalCount" => $query->post_count,
            "postType" => get_post_type(),
            "postType2" => $query->post->post_type,
            "image" => get_the_post_thumbnail_url(0, 'profPortrait'),
            "pageBannerImage" => get_field('page_banner_background_image')['sizes']['pageBanner'], // get custom field data
            "relatedPrograms" => get_field('related_programs'),
            // "allData" => $query->post,
            // "relatedTypeData" => getRelatedTypeData() // Related datayı bu şekilde de alabilirsin,
            // ancak getirilecek verinin tamamı fazla boyut kaplamadığından önyüzde cacheleyip, orada birleştirmeyi tercih ettim.
        ));
    }

    // $query->post => o anki post, post nesnesi döndürüyor.
    // $query->post_count => row count veriyor
    // $query->posts => tüm rowları array olarak veriyor.

    return $searchReuslts;
}

function getRelatedTypesData() {
    $query = new WP_Query(array(
        'posts_per_page' => -1,
        'post_type' => array("event","professor","program"),
    ));

    $searchReuslts = array();
    while($query->have_posts()){
        $query->the_post();
        array_push($searchReuslts, array( // sonuç dizimizi oluşturuyoruz.
            "id" => $query->post->ID,
            "title" => get_the_title(),
            "permalink" => get_the_permalink(),
            "postType" => $query->post->post_type, // get_post_type() ile aynı şey
            "image" => get_the_post_thumbnail_url(0, 'profPortrait'),
            "pageBannerImage" => get_field('page_banner_background_image')['sizes']['pageBanner'],
            // "allData" => $query->post,
        ));
    }

    return $searchReuslts;

}


?>