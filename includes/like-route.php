<?php

//Build your own rest api endpoints here! 

add_action('rest_api_init','likeRoutes');

function likeRoutes()
{
    register_rest_route('api/v1', 'manageLike', array(
        'methods' => 'POST',
        'callback' => 'addLike'
    ));

    register_rest_route('api/v1', 'manageLike', array(
        'methods' => 'DELETE', 
        'callback' => 'deleteLike'
    ));
}

function isThisUserLikedBefore($userId, $profId) {
    global $wpdb;
    $results = $wpdb->get_results(
        'SELECT * FROM `wp_postmeta` m, `wp_posts` p 
            WHERE meta_key = "liked_professor_id" 
            and p.ID = m.post_id 
            and p.post_status="publish" 
            and p.post_author = $userId
            and m.meta_value = $profId
        ');    
    return count($results) > 0;
}

function addLike($queryStr) {

    if (!is_user_logged_in()) {
        die("You need to be logged in in order to give a like!");
    }

    $profId = sanitize_text_field($queryStr["professorId"]);

    if (isThisUserLikedBefore(get_current_user_id(), $profId)) {
        die("You like a proffessor just for once");
    }

    if (get_post_type($profId)!= "professor") {
        die("Prof dışında başka bir post tipi için beğeni yapamazsınız.");
    }

    // Kodla yeni post kaydı ekle...
    $newPostId = wp_insert_post(array(
        'post_type' => 'like',
        'post_status' => 'publish',
        'post_title' => 'Php Create Third Like',
        'meta_input' => array( // custom fieldlara değer kaydetmek bu kadar basit :) 
            'liked_professor_id' => sanitize_text_field($profId)
        )
    ));

    return array(
        "likeId" => $newPostId,
        "message" => "Yeni beğeni başarıyla eklendi",
    );
}

function deleteLike($queryStr){
    $likeId = sanitize_text_field($queryStr["likeId"]);

    // get_post_field("fieldName", postId) => ile ilgili yazarın numarasını alıyoruz veeee
    // eğer bu mevcut kullanıcı ile uyumlu ise 
    if ( get_current_user_id() == get_post_field('post_author', $likeId)
         AND get_post_type($likeId) == "like" // ve idsi verilen post tipi gerçekten like ise
    ) {
        wp_delete_post($likeId, true); // çöpe göndermeden doğrudan sil
        return "Like deleted successfuly";
    } else {
        die("You do not have permission to delete this like!");
    }
    
}
 
?>